<?php

namespace App\DTO;

use Illuminate\Contracts\Support\Arrayable;

final class OfferDto implements Arrayable
{

    #[DtoRule(max: 255)]
    /**
     * @var string
     */
    public string $title;

    #[DtoRule(min: 1000, max: 999999999)]
    /**
     * @var integer
     */
    public int $price;

    #[DtoRule(max: 4096)]
    /**
     * @var nullable
     */
    public ?string $description;

    /**
     * @var bool
     */
    public bool $isActive;

    #[DtoRule(date_format: 'Y-m-d H:i:s')]
    /**
     * @var string
     */
    public string $publishAt;


    public function __construct(string $title, int $price, ?string $description, bool $isActive, string $publishAt)
    {
        $this->title = $title;
        $this->price = $price;
        $this->description = $description;
        $this->isActive = $isActive;
        $this->publishAt = $publishAt;
    }

    /**
     * @return array[]
     */
    public function toArray(): array
    {
        return [
             'title' => $this->title,
             'price' => $this->price,
             'description' => $this->description,
             'isActive' => $this->isActive,
             'publishAt' => $this->publishAt,
        ];
    }
}
