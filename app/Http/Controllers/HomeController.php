<?php

namespace App\Http\Controllers;

use App\Http\Requests\OfferCreateFormRequest;

class HomeController extends Controller
{
    public function create(OfferCreateFormRequest $request)
    {
        return $request->toDto();
    }
}
