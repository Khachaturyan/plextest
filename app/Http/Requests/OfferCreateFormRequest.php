<?php

namespace App\Http\Requests;

use App\DTO\OfferDto;
use Illuminate\Foundation\Http\FormRequest;

class OfferCreateFormRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     * @throws \ReflectionException
     */
    public function rules(): array
    {
        $offerDtoClass = OfferDto::class;
        $reflectionClass = new \ReflectionClass($offerDtoClass);
        $reflectionProperties = $reflectionClass->getProperties();

        $rules = [];

        foreach ($reflectionProperties as $reflectionProperty) {
            $propertyName = $reflectionProperty->getName();
            $propertyAnnotations = $reflectionProperty->getAttributes();
            if ($reflectionProperty->getDocComment()) {
                if (preg_match('/@var\s+([^\s]+)/', $reflectionProperty->getDocComment(), $matches)) {
                    list(, $type) = $matches;
                }
            }

            $propertyRules = [];

            foreach ($propertyAnnotations as $propertyAnnotation) {
                $ruleArguments = $propertyAnnotation->getArguments();
                $ruleArray = [];
                foreach ($ruleArguments as $key => $val) {
                    $ruleArray = array_merge($ruleArray, [$ruleArray = $key . ':' . $val]);
                }
                $propertyRules = $ruleArray;
                array_push($propertyRules, $type);
                if ($type !== 'nullable') {
                    array_push($propertyRules, 'required');
                }
            }

            $rules[$propertyName] = $propertyRules;
        }
        return $rules;
    }

    public function toDto()
    {
        $offerDto = new OfferDto($this->title, $this->price, $this->description, $this->isActive, $this->publishAt);
        return $offerDto;
    }

}
