<?php

namespace App\Rules;


class DtoRule
{

    /**
     * @var int|null
     */
    private ?int $min;

    /**
     * @var int|null
     */
    private ?int $max;

    /**
     * @var string|null
     */
    private ?string $date_format;

    /**
     * DtoRule constructor.
     * @param int|null $min
     * @param int|null $max
     * @param string|null $date_format
     */
    public function __construct(?int $min = null, ?int $max = null, ?string $date_format = null)
    {
        $this->min = $min;
        $this->max = $max;
        $this->date_format = $date_format;
    }
}
